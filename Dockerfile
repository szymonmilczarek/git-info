FROM gradle:7.0.2-jdk11 as build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build -x test -x integrationTest

FROM openjdk:11.0.9-jdk-oracle

RUN mkdir /app
COPY --from=build /home/gradle/src/build/libs/git-info-0.0.1-SNAPSHOT.jar /app/git-info-0.0.1-SNAPSHOT.jar
WORKDIR /app

CMD ["sh", "-c", "java -Dspring.profiles.active=${SPRING_PROFILE_ACTIVE} -jar /app/git-info-0.0.1-SNAPSHOT.jar"]

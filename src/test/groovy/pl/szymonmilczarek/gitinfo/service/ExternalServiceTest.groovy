package pl.szymonmilczarek.gitinfo.service

import pl.szymonmilczarek.gitinfo.utils.RetrofitCallExecutor
import spock.lang.Specification

class ExternalServiceTest extends Specification {

    def "throws NullPointerException when call executor is null"() {
        when:
            new ExternalService(null)
        then:
            thrown(NullPointerException)
    }

    def "given executor is bound"(executor) {
        given:
            def service = new ExternalService(executor)
        expect:
            service.executor == executor
        where:
            executor << [
                    Mock(RetrofitCallExecutor),
                    new RetrofitCallExecutor(ExternalService)
            ]
    }
}

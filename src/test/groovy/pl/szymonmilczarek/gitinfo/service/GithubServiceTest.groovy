package pl.szymonmilczarek.gitinfo.service

import pl.szymonmilczarek.gitinfo.controller.mapper.RepositoryInfoDTOMapper
import pl.szymonmilczarek.gitinfo.dao.entity.RequestCountEntity
import pl.szymonmilczarek.gitinfo.dao.repository.RequestCountRepository
import pl.szymonmilczarek.gitinfo.facade.GithubServiceFacade
import pl.szymonmilczarek.gitinfo.facade.dto.GithubRepositoryInfoDTO
import retrofit2.mock.Calls
import spock.lang.Specification

import java.math.MathContext
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class GithubServiceTest extends Specification {

    def "throws NullPointerException when null is given"(githubServiceFacade, mapper, repository) {
        when:
            new GithubService(githubServiceFacade, mapper, repository)
        then:
            thrown(NullPointerException)
        where:
            githubServiceFacade       | mapper                        | repository
            Mock(GithubServiceFacade) | null                          | null
            null                      | Mock(RepositoryInfoDTOMapper) | null
            null                      | null                          | Mock(RequestCountRepository)
            null                      | null                          | null
    }

    def "should return githubRepositoryDTO - first request"(BigDecimal followers, BigDecimal publicRepos, BigDecimal calculations) {
        given:
            def date = LocalDateTime.parse("2011-01-26T19:01:12Z", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'"))
            def input = new GithubRepositoryInfoDTO()
            input.id = 583231
            input.login = "octocat"
            input.name = "The Octocat"
            input.type = "User"
            input.avatarUrl = new URL("https://avatars.githubusercontent.com/u/583231?v=4")
            input.createdAt = date
            input.followers = followers
            input.publicRepos = publicRepos

            def facade = Mock(GithubServiceFacade) {
                1 * getGithubRepositoryInfo("octocat") >> Calls.response(Spy(GithubRepositoryInfoDTO) {
                    it.id = input.id
                    it.login = input.login
                    it.name = input.name
                    it.type = input.type
                    it.avatarUrl = input.avatarUrl
                    it.createdAt = input.createdAt
                    it.followers = input.followers
                    it.publicRepos = input.publicRepos
                })
            }
            def mapper = new RepositoryInfoDTOMapper()
            def repository = Mock(RequestCountRepository) {
                1 * findByLogin(input.login) >> Optional.empty()
                1 * save({
                    verifyAll(it, RequestCountEntity) {
                        it.id == null
                        it.login == "octocat"
                        it.requestCount == 1
                    }
                })
            }
            def githubService = Spy(new GithubService(facade, mapper, repository))
        when:
            def r = githubService.getGithubRepositoryInfo("octocat")
        then:
            1 * githubService.execute(_)
            r.id == input.id
            r.login == input.login
            r.name == input.name
            r.type == input.type
            r.avatarUrl == input.avatarUrl
            r.createdAt == input.createdAt
            r.createdAt == date
            r.calculations == calculations
        where:
            followers | publicRepos | calculations
            0         | 15          | 0
            12345     | 123         | new BigDecimal(0.06075).round(new MathContext(4))
            56897     | 99          | new BigDecimal(0.01065).round(new MathContext(4))
    }

    def "should return githubRepositoryDTO - next request"(BigDecimal followers, BigDecimal publicRepos, BigDecimal calculations) {
        given:
            def date = LocalDateTime.parse("2011-01-26T19:01:12Z", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'"))
            def requestEntity = new RequestCountEntity()
            requestEntity.id = 1L
            requestEntity.requestCount = 10
            requestEntity.login = "octocat"

            def input = new GithubRepositoryInfoDTO()
            input.id = 583231
            input.login = "octocat"
            input.name = "The Octocat"
            input.type = "User"
            input.avatarUrl = new URL("https://avatars.githubusercontent.com/u/583231?v=4")
            input.createdAt = date
            input.followers = followers
            input.publicRepos = publicRepos

            def facade = Mock(GithubServiceFacade) {
                1 * getGithubRepositoryInfo("octocat") >> Calls.response(Spy(GithubRepositoryInfoDTO) {
                    it.id = input.id
                    it.login = input.login
                    it.name = input.name
                    it.type = input.type
                    it.avatarUrl = input.avatarUrl
                    it.createdAt = input.createdAt
                    it.followers = input.followers
                    it.publicRepos = input.publicRepos
                })
            }
            def mapper = new RepositoryInfoDTOMapper()
            def repository = Mock(RequestCountRepository) {
                1 * findByLogin(input.login) >> Optional.of(requestEntity)
                1 * save({
                    verifyAll(it, RequestCountEntity) {
                        it.id == requestEntity.id
                        it.login == requestEntity.login
                        it.requestCount == requestEntity.requestCount
                    }
                })
            }
            def githubService = Spy(new GithubService(facade, mapper, repository))
        when:
            def r = githubService.getGithubRepositoryInfo("octocat")
        then:
            1 * githubService.execute(_)
            r.id == input.id
            r.login == input.login
            r.name == input.name
            r.type == input.type
            r.avatarUrl == input.avatarUrl
            r.createdAt == input.createdAt
            r.createdAt == date
            r.calculations == calculations
        where:
            followers | publicRepos | calculations
            0         | 15          | 0
            12345     | 123         | new BigDecimal(0.06075).round(new MathContext(4))
            56897     | 99          | new BigDecimal(0.01065).round(new MathContext(4))
    }
}

package pl.szymonmilczarek.gitinfo.controller.mapper

import pl.szymonmilczarek.gitinfo.facade.dto.GithubRepositoryInfoDTO
import spock.lang.Specification

import java.math.MathContext
import java.math.RoundingMode
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class RepositoryInfoDTOMapperTest extends Specification {

    def "returns null when null is given"() {
        given:
            def m = new RepositoryInfoDTOMapper()
        when:
            def r = m.apply(null)
        then:
            r == null
    }

    def "maps properly"(BigDecimal followers, BigDecimal publicRepos, BigDecimal calculations) {
        given:
            def date = LocalDateTime.parse("2011-01-26T19:01:12Z", DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'"))
            def input = new GithubRepositoryInfoDTO()
            input.id = 583231
            input.login = "octocat"
            input.name = "The Octocat"
            input.type = "User"
            input.avatarUrl = new URL("https://avatars.githubusercontent.com/u/583231?v=4")
            input.createdAt = date
            input.followers = followers
            input.publicRepos = publicRepos
            def m = new RepositoryInfoDTOMapper()
        when:
            def r = m.apply(input)
        then:
            r.id == input.id
            r.login == input.login
            r.name == input.name
            r.type == input.type
            r.avatarUrl == input.avatarUrl
            r.createdAt == input.createdAt
            r.createdAt == date
            r.calculations == calculations
        where:
            followers | publicRepos | calculations
            0         | 15          | 0
            12345     | 123         | new BigDecimal(0.06075).round(new MathContext(4))
            56897     | 99          | new BigDecimal(0.01065).round(new MathContext(4))

    }

}

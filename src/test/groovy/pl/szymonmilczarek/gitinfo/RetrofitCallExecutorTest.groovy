package pl.szymonmilczarek.gitinfo

import okhttp3.Headers
import okhttp3.HttpUrl
import okhttp3.MediaType
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException
import pl.szymonmilczarek.gitinfo.utils.RetrofitCallExecutor
import retrofit2.Call
import retrofit2.Response
import spock.lang.Specification

class RetrofitCallExecutorTest extends Specification {

    def "throws NullPointerException when target is null"() {
        when:
            new RetrofitCallExecutor(null)
        then:
            thrown(NullPointerException)
    }

    def "throws NullPointerException when call is null"() {
        given:
            def executor = new RetrofitCallExecutor(Specification)
        when:
            executor.executeOrThrow(null)
        then:
            thrown(NullPointerException)
    }

    def "throws ResponseStatusException when call response status code is different than 200"(int statusCode, int expectedStatusCode) {
        given:
            def executor = new RetrofitCallExecutor(Specification)
        when:
            executor.executeOrThrow(errorStubCall(statusCode))
        then:
            def exception = thrown(ResponseStatusException)
            exception.status.value() == expectedStatusCode
        where:
            statusCode                               | expectedStatusCode
            HttpStatus.FORBIDDEN.value() | HttpStatus.FORBIDDEN.value()
            HttpStatus.UNAUTHORIZED.value()          | HttpStatus.UNAUTHORIZED.value()
            HttpStatus.INTERNAL_SERVER_ERROR.value() | HttpStatus.INTERNAL_SERVER_ERROR.value()
            HttpStatus.SERVICE_UNAVAILABLE.value()   | HttpStatus.SERVICE_UNAVAILABLE.value()
            HttpStatus.NOT_FOUND.value()             | HttpStatus.NOT_FOUND.value()
            900                                      | HttpStatus.I_AM_A_TEAPOT.value()
    }

    def "proceeds when response is sort of success"(int statusCode, int expectedStatusCode) {
        given:
            def executor = new RetrofitCallExecutor(Specification)
        when:
            def result = executor.executeOrThrow(okStubCall(statusCode))
        then:
            result.code() == expectedStatusCode
        where:
            statusCode                    | expectedStatusCode
            HttpStatus.OK.value()         | HttpStatus.OK.value()
            HttpStatus.ACCEPTED.value()   | HttpStatus.ACCEPTED.value()
            HttpStatus.CREATED.value()    | HttpStatus.CREATED.value()
            HttpStatus.NO_CONTENT.value() | HttpStatus.NO_CONTENT.value()
            299                           | 299
    }

    def "given target class is equal to the given one"(Class target) {
        given:
            def executor = new RetrofitCallExecutor(target)
        expect:
            executor.target == target
        where:
            target << [Specification, RetrofitCallExecutorTest]
    }

    Call okStubCall(int statusCode) {
        return callStub(Response.success(statusCode, json()))
    }

    Call errorStubCall(int statusCode) {
        return callStub(Response.error(statusCode, json()))
    }

    Call callStub(Response response) {
        def call = Mock(Call)
        call.execute() >> response
        def request = new Request(
                new HttpUrl.Builder().scheme("http").host("some.url").build(),
                "GET",
                Headers.of(Collections.<String, String> emptyMap()),
                Mock(RequestBody),
                Mock(Map))
        call.request() >> request
        return call
    }

    ResponseBody json() {
        return json("{}")
    }

    ResponseBody json(String json) {
        return ResponseBody.create(json, MediaType.parse("application/json"))
    }
}

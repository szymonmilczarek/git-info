create table request
(
    id            bigserial primary key,
    login         varchar(255),
    request_count int
);

package pl.szymonmilczarek.gitinfo.controller;

import pl.szymonmilczarek.gitinfo.facade.dto.RepositoryInfoDTO;
import pl.szymonmilczarek.gitinfo.service.GithubService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class GithubController {

    private final GithubService githubService;

    public GithubController(
            GithubService githubService
    ) {
        this.githubService = githubService;
    }

    @GetMapping("/users/{login}")
    public RepositoryInfoDTO getGithubRepositoryInfo(
            @PathVariable String login
    ) {
        return githubService.getGithubRepositoryInfo(login);
    }


}

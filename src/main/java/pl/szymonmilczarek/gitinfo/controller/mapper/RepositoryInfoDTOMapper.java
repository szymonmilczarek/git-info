package pl.szymonmilczarek.gitinfo.controller.mapper;

import org.springframework.stereotype.Component;
import pl.szymonmilczarek.gitinfo.facade.dto.GithubRepositoryInfoDTO;
import pl.szymonmilczarek.gitinfo.facade.dto.RepositoryInfoDTO;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.function.Function;

@Component
public class RepositoryInfoDTOMapper implements Function<GithubRepositoryInfoDTO, RepositoryInfoDTO> {

    @Override
    public RepositoryInfoDTO apply(GithubRepositoryInfoDTO in) {
        if (in == null) {
            return null;
        }
        RepositoryInfoDTO out = new RepositoryInfoDTO();
        out.id = in.id;
        out.login = in.login;
        out.name = in.name;
        out.type = in.type;
        out.avatarUrl = in.avatarUrl;
        out.createdAt = in.createdAt;

        if (!in.followers.equals(new BigDecimal(0))) {
            out.calculations =
                    new BigDecimal(6)
                            .divide(in.followers, 10, RoundingMode.HALF_EVEN)
                            .multiply(new BigDecimal(2).add(in.publicRepos))
                            .round(new MathContext(4)
                    );
        } else {
            out.calculations = new BigDecimal(0);

        }
        return out;
    }
}

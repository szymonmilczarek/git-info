package pl.szymonmilczarek.gitinfo.dao.repository;

import pl.szymonmilczarek.gitinfo.dao.entity.RequestCountEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.Optional;

public interface RequestCountRepository extends CrudRepository<RequestCountEntity, Long> {

    @Override
    Collection<RequestCountEntity> findAll();

    Optional<RequestCountEntity> findByLogin(String login);
}

package pl.szymonmilczarek.gitinfo.dao.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "request")
public class RequestCountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "login")
    private String login;

    @Column(name = "request_count")
    private Integer requestCount;

    public RequestCountEntity(String login, Integer requestCount) {
        this.login = login;
        this.requestCount = requestCount;
    }

    public void incrementRequestCount() {
        requestCount++;
    }
}

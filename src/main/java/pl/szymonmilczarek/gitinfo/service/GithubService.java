package pl.szymonmilczarek.gitinfo.service;

import pl.szymonmilczarek.gitinfo.controller.mapper.RepositoryInfoDTOMapper;
import pl.szymonmilczarek.gitinfo.dao.entity.RequestCountEntity;
import pl.szymonmilczarek.gitinfo.dao.repository.RequestCountRepository;
import pl.szymonmilczarek.gitinfo.facade.GithubServiceFacade;
import pl.szymonmilczarek.gitinfo.facade.dto.RepositoryInfoDTO;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class GithubService extends ExternalService {

    private final GithubServiceFacade serviceFacade;
    private final RepositoryInfoDTOMapper mapper;
    private final RequestCountRepository repository;

    public GithubService(
            GithubServiceFacade serviceFacade,
            RepositoryInfoDTOMapper mapper,
            RequestCountRepository repository

    ) {
        this.serviceFacade = Objects.requireNonNull(serviceFacade);
        this.mapper = Objects.requireNonNull(mapper);
        this.repository = Objects.requireNonNull(repository);
    }

    public RepositoryInfoDTO getGithubRepositoryInfo(String login) {
        var githubRepositoryInfoDTO = execute(serviceFacade.getGithubRepositoryInfo(login));
        saveRequestCount(login);
        return mapper.apply(githubRepositoryInfoDTO);
    }

    private void saveRequestCount(String login) {
        var requestCountEntity = getRequestCount(login);
        requestCountEntity.incrementRequestCount();
        repository.save(requestCountEntity);
    }

    private RequestCountEntity getRequestCount(String login) {
        return repository.findByLogin(login).orElseGet(() -> new RequestCountEntity(login, 0));
    }
}

package pl.szymonmilczarek.gitinfo.service;

import pl.szymonmilczarek.gitinfo.utils.RetrofitCallExecutor;
import retrofit2.Call;

import java.util.Objects;

public class ExternalService {
    protected RetrofitCallExecutor executor;

    public ExternalService(RetrofitCallExecutor executor) {
        this.executor = Objects.requireNonNull(executor);
    }

    public ExternalService() {
        this.executor = new RetrofitCallExecutor(getClass());
    }

    protected <T> T execute(Call<T> call) {
        return executor.execOrThrow(call);
    }
}

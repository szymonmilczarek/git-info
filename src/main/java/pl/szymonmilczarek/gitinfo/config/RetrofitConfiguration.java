package pl.szymonmilczarek.gitinfo.config;

import pl.szymonmilczarek.gitinfo.facade.GithubServiceFacade;
import pl.szymonmilczarek.gitinfo.utils.RetrofitUtils;
import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RetrofitConfiguration {

    @Bean
    public GithubServiceFacade githubServiceFacade(
            HttpClientFactory httpClientFactory,
            @Value("${integration.github.service.url}") String url
    ) {

        OkHttpClient client = httpClientFactory
                .builder()
                .build();
        return RetrofitUtils.service(client, url, GithubServiceFacade.class);
    }
}

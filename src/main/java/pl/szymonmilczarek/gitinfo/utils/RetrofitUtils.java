package pl.szymonmilczarek.gitinfo.utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public final class RetrofitUtils {

    private RetrofitUtils() {
    }

    private static Retrofit retrofit(OkHttpClient client, String url) {
        if (url != null && !url.endsWith("/")) {
            url = String.format("%s/", url);
        }
        if (url == null) {
            url = "";
        }
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.enable(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_USING_DEFAULT_VALUE);
        return new Retrofit.Builder()
                .baseUrl(url)
                .client(client)
                .addConverterFactory(JacksonConverterFactory.create(mapper))
                .build();
    }

    public static <T> T service(OkHttpClient client, String host, Class<T> clazz) {
        return retrofit(client, host).create(clazz);
    }
}

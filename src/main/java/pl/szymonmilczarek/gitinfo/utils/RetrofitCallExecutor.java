package pl.szymonmilczarek.gitinfo.utils;

import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import org.springframework.web.server.ResponseStatusException;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.Objects;

import static java.util.Optional.ofNullable;
import static org.springframework.http.HttpStatus.*;

public class RetrofitCallExecutor {
    protected Class target;

    public RetrofitCallExecutor(Class target) {
        this.target = Objects.requireNonNull(target);
    }

    public <T> T execOrThrow(Call<T> call) {
        return executeOrThrow(call).body();
    }

    public <T> Response<T> executeOrThrow(Call<T> call) {
        try {
            Response<T> response = Objects.requireNonNull(call).execute();
            if (!response.isSuccessful()) {
                throw new ResponseStatusException(ofNullable(resolve(response.code())).orElse(I_AM_A_TEAPOT),
                        String.format("Unable to invoke external service: %s, response message: %s",
                                target.getSimpleName(),
                                response.errorBody() != null ? response.errorBody().string() : response.message()));
            }
            return response;
        } catch (UnrecognizedPropertyException e) {
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR, e.getMessage(), e);
        } catch (IOException e) {
            throw new ResponseStatusException(SERVICE_UNAVAILABLE, e.getMessage(), e);
        }
    }
}

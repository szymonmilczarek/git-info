package pl.szymonmilczarek.gitinfo.facade.dto;

import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDateTime;

public class RepositoryInfoDTO {

    public Long id;
    public String login;
    public String name;
    public String type;
    public URL avatarUrl;
    public LocalDateTime createdAt;
    public BigDecimal calculations;
}

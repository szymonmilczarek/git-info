package pl.szymonmilczarek.gitinfo.facade.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GithubRepositoryInfoDTO {

    @JsonProperty("id")
    public Long id;

    @JsonProperty("login")
    public String login;

    @JsonProperty("name")
    public String name;

    @JsonProperty("type")
    public String type;

    @JsonProperty("avatar_url")
    public URL avatarUrl;

    @JsonProperty("created_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    public LocalDateTime createdAt;

    @JsonProperty("followers")
    public BigDecimal followers;

    @JsonProperty("public_repos")
    public BigDecimal publicRepos;

}

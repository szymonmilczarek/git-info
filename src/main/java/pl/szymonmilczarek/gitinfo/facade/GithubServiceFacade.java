package pl.szymonmilczarek.gitinfo.facade;

import pl.szymonmilczarek.gitinfo.facade.dto.GithubRepositoryInfoDTO;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GithubServiceFacade {

    @GET("/users/{owner}")
    Call<GithubRepositoryInfoDTO> getGithubRepositoryInfo(
            @Path("owner") String owner);

}

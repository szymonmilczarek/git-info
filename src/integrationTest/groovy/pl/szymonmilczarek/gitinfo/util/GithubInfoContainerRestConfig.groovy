package pl.szymonmilczarek.gitinfo.util

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.Network
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.containers.output.Slf4jLogConsumer
import org.testcontainers.containers.wait.strategy.Wait
import org.testcontainers.spock.Testcontainers

import java.util.function.Supplier

@Testcontainers
class GithubInfoContainerRestConfig extends GithubInfoRestConfig {

    static final PostgreSQLContainer<?> postgres

    static final Logger logger = LoggerFactory.getLogger(GithubInfoContainerRestConfig.class)

    static {
        Network network = Network.newNetwork()

        postgres = new PostgreSQLContainer<>("postgres:13.3-alpine")
                .withUsername("git-info")
                .withPassword("git-info")
                .withDatabaseName("git-info")
                .withNetworkAliases("git-info")
                .withExposedPorts(9999)
                .withLogConsumer(new Slf4jLogConsumer(logger))
                .waitingFor(Wait.forLogMessage(".*database system is ready to accept connections.*\\s", 1))
                .withNetwork(network)
        postgres.start()
    }

    @DynamicPropertySource
    static void initializeDatasource(DynamicPropertyRegistry registry) {
        String dbUrl = String.format(POSTGRES_URL_BASE_TEMPLATE, postgres.getHost(), postgres.getLivenessCheckPortNumbers().iterator().next())
        registry.add("spring.datasource.url", { dbUrl } as Supplier)
    }

    def mapper

    void setup() {
        mapper = new ObjectMapper()
        mapper.registerModule(new JavaTimeModule())
    }
}

package pl.szymonmilczarek.gitinfo.controller

import org.springframework.http.HttpStatus
import org.springframework.test.context.jdbc.Sql
import pl.szymonmilczarek.gitinfo.facade.dto.RepositoryInfoDTO
import pl.szymonmilczarek.gitinfo.util.GithubInfoContainerRestConfig
import pl.szymonmilczarek.gitinfo.util.GithubInfoSpringBootApplicationTest

import java.math.MathContext
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@GithubInfoSpringBootApplicationTest
@Sql(scripts = "classpath:/seed/clear_data.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(scripts = "classpath:/seed/github_data.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class GithubControllerIT extends GithubInfoContainerRestConfig {

    def "resource is available"() {
        given:
            def response = httpGet("api/users/octocat")
        expect:
            response.statusCode == HttpStatus.OK
    }

    def "getGithubRepositoryInfo returns repositoryInfo"() {
        given:
            def response = httpGet("api/users/octocat")
        expect:
            response.statusCode.value() == HttpStatus.OK.value()
            def result = mapper.readValue(response.body, RepositoryInfoDTO.class)
            result.id == 583231
            result.login == "octocat"
            result.name == "The Octocat"
            result.type == "User"
            result.avatarUrl == new URL("https://avatars.githubusercontent.com/u/583231?v=4")
            def formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")
            result.createdAt == LocalDateTime.parse("2011-01-25T18:44:36Z", formatter)
            result.calculations == new BigDecimal(0.01572).round(new MathContext(4))

    }

}


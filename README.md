# GITHUB REPOSITORY INFO APP

### Build application:

```
gradle build -x test
```

### Run unit tests:

```
gradle test
```

### Run integration tests:

```
gradle integrationTest
```

- integration Tests require Docker

### Running application in development mode:

Run application with spring profile `dev`. This can be achieved by:

- Providing active profile in spring boot run config
- Adding this: `-Dspring.profiles.active=dev` to vm options.

Run local postgres database in docker container:

```
docker-compose -f docker-compose.yml up --force-recreate --build -d git-info-postgres
```

### Running application as docker container (http://localhost:8080/):

```
docker-compose -f docker-compose.yml up --force-recreate --build
```
